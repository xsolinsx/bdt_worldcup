import csv
import datetime

import requests
from bs4 import BeautifulSoup

header = [
    "Match_Number",
    "Country",
    "Country_Code",
    "Goals_Scored",
    "Date",
    "Points_Gained",
    "Group",
]
links = [
    ("https://www.fifa.com/worldcup/archive/southafrica2010/matches/", 10),
    ("https://www.fifa.com/worldcup/archive/brazil2014/matches/", 14),
    ("https://www.fifa.com/worldcup/archive/russia2018/matches/", 18),
]
ds_dir = "./datasets/"

# run through all world cups
for link, year in links:
    source = requests.get(link).text
    soup = BeautifulSoup(source, "lxml")

    groups = soup.find_all(
        "div", class_="fi-groupPhase fi__info__group fi-ltr--force"
    )  # contain group
    # add None where group is not applicable (knockout phase)
    for i in range(len(groups), 64):
        groups.append(None)

    # get required info and combine them into tuples
    matches = zip(
        soup.find_all("div", class_="fi-t fi-i--4 home"),  # contain first team
        soup.find_all("div", class_="fi-s-wrap"),  # contain result and date
        soup.find_all("div", class_="fi-t fi-i--4 away"),  # contain second team
        groups,
        soup.find_all("div", class_="fi__info__matchnumber"),  # contain match number
    )
    # country1, result_date, country2, group, match

    # split tuples and adjust some values
    matches = [
        (
            match.text.replace("\n", "").replace("Match", "").strip(),  # match
            c1.text.replace("\n", "").strip()[:-3],  # c1
            c1.text.replace("\n", "").strip()[-3:],  # c1_code
            match_info.text.replace("\n", "").strip().split("\r")[0],  # date
            group.text.replace("\n", "").replace("Group", "").strip()
            if group
            else None,  # group
            match_info.text.replace("\n", "").strip().split("\r")[1],  # score
            c2.text.replace("\n", "").strip()[:-3],  # c2
            c2.text.replace("\n", "").strip()[-3:],  # c2_code
        )
        for c1, match_info, c2, group, match in matches
    ]
    # match, country1, country1_code, date, group, score, country2, country2_code

    # split score and order data
    matches = [
        (
            match,
            c1,
            c1_code,
            score.split("-")[0],
            date,
            c2,
            c2_code,
            score.split("-")[1],
            group,
        )
        for match, c1, c1_code, date, group, score, c2, c2_code in matches
    ]
    # match, country1, country1_code, country1_goals, date, country2, country2_code, country2_goals, group

    # order by match
    matches.sort(key=lambda x: int(x[0]))

    # each match is separated in 2 lines, one for each team
    double_matches = []
    for i in range(len(matches)):
        double_matches.append(
            (
                matches[i][0],  # match_number
                matches[i][1],  # c1
                matches[i][2],  # c1_code
                matches[i][3],  # goals_scored
                datetime.datetime.strptime(
                    f"{matches[i][4]} {year}", "%d %B %y"
                ).date(),  # normalize date
                1
                if int(matches[i][3]) - int(matches[i][7]) == 0
                else (
                    3 if int(matches[i][3]) > int(matches[i][7]) else 0
                ),  # points gained (0 lose, 1 draw, 3 win)
                matches[i][8],  # group
            )
        )
        double_matches.append(
            (
                matches[i][0],  # match_number
                matches[i][5],  # c2
                matches[i][6],  # c2_code
                matches[i][7],  # goals_scored
                datetime.datetime.strptime(
                    f"{matches[i][4]} {year}", "%d %B %y"
                ).date(),  # normalize date
                1
                if int(matches[i][3]) == int(matches[i][7])
                else (
                    0 if int(matches[i][3]) > int(matches[i][7]) else 3
                ),  # points gained (0 lose, 1 draw, 3 win)
                matches[i][8],  # group
            )
        )

    # open a new csv with newline "\n" and encoding utf-8-sig to avoid text corruption
    with open(
        f"{ds_dir}matches{year}.csv", "w", newline="\n", encoding="utf-8-sig",
    ) as csv_file:
        csv_writer = csv.writer(csv_file)
        # write header
        csv_writer.writerow(header)
        csv_writer.writerows(double_matches)
