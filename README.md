# Basic Instructions

1. Install Python 3.7.0
2. Install R 3.6.2
3. Install Python libraries with
    - Windows: `pip install -r requirements.txt`
    - Linux: `pip3 install -r requirements.txt`
4. Run [scrape_world_cup_matches.py](../master/scrape_world_cup_matches.py) or use the data already provided.</li>
5. Run [scrape_world_cup_players.py](../master/scrape_world_cup_players.py) or use the data already provided.</li>
6. Run [scrape_futbin.py](../master/scrape_futbin.py) or use the data already provided.</li>
7. Execute [data_preparation.ipynb](../master/data_preparation.ipynb) up to point 2 included</li>
8. Run [scrape_normal_detailed_futbin.py](../master/scrape_normal_detailed_futbin.py) or use the data already provided.
9. Execute remaining part of [data_preparation.ipynb](../master/data_preparation.ipynb) (from point 3 onwards)
10. Open [data_analysis.Rmd](../master/data_analysis.Rmd)
11. Install missing libraries if prompted by RStudio
12. Execute [data_analysis.Rmd](../master/data_analysis.Rmd)
