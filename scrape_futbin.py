import csv
import random
import re
import time

import requests
from bs4 import BeautifulSoup

# columns on futbin.com are: NAME,RATING,POSITION,VERSION,PS_PRICE,SKILLS,WEAK_FOOT,ATK_DEF_WORK_RATE,PACE,SHOOTING,PASSING,DEFENDING,PHYSICALITY,HEIGHT,POPULARITY,BASE_SCORE,IN_GAME_STATS
header = [
    "Player_ID",
    "Link_Name",
    "Club_ID",
    "Club_Name",
    "Country_ID",
    "Country_Name",
    "Player_Name",
    "Rating",
    "Position",
    "Version",
    "PS_Price",
    "Skills",
    "Weak_Foot",
    "Attack_Defense_Work_Rate",
    "Pace",
    "Shooting",
    "Passing",
    "Dribbling",
    "Defending",
    "Physicality",
    "Height",
    "Popularity",
    "Base_Score",
    "In_Game_Stats",
]
ds_dir = "./datasets/"

# regex used to match player's id and name
regex_player = re.compile(r"^/\d+/\w+/(\d+)/(.+)", re.I)
# regex used to match player's club and its id
regex_club = re.compile(r"(\d+)$", re.I)
# regex used to match player's country and its id
regex_country = re.compile(r"(\d+)$", re.I)

# run through all fifa versions of world cups
for year in range(10, 21, 4):
    source = requests.get(f"https://www.futbin.com/{year}/players?page=1").text
    soup = BeautifulSoup(source, "lxml")

    # find number of last page in the paginator, only the second-last <li> has last page
    last_page = int(
        soup.find("ul", class_="pagination pg-blue justify-content-end")
        .find_all("li")[-2]
        .a.text
    )

    # open a new csv with newline "\n" and encoding utf-8-sig to avoid text corruption
    with open(
        f"{ds_dir}players{year}.csv", "w", newline="\n", encoding="utf-8-sig"
    ) as csv_file:
        csv_writer = csv.writer(csv_file)

        # write header
        csv_writer.writerow(header)

        # list of rows to write
        buffer = []

        # cycle through pages
        for page in range(1, last_page + 1):
            print(f"fifa{year} {page}/{last_page}")
            source = requests.get(
                f"https://www.futbin.com/{year}/players?page={page}"
            ).text
            soup = BeautifulSoup(source, "lxml")

            # get players table
            players_table = soup.find("table", id="repTb").tbody

            # find all table rows
            for tr in players_table.find_all("tr"):
                # get all table columns of the current row (i.e. all cells)
                row = tr.find_all("td")

                # if page has records then continue, otherwise skip
                if row[0].attrs["class"][0] != "no_results":
                    # keep text and strip it
                    player_data = [x.text.strip() for x in row]
                    # get player id and link name
                    link_to_player = (
                        row[0]
                        .find_all("div")[1]
                        .find_all("div")[0]
                        .find_all("a")[0]
                        .attrs["href"]
                    )
                    player_id, player_link_name = regex_player.findall(link_to_player)[
                        0
                    ]

                    # get club id and name
                    link_to_club = (
                        row[0]
                        .find_all("div")[1]
                        .find_all("div")[1]
                        .find_all("span")[0]
                        .find_all("a")[0]
                    )
                    club_id, club_name = (
                        regex_club.findall(link_to_club.attrs["href"])[0],
                        link_to_club.attrs["data-original-title"],
                    )

                    # get country id and name
                    link_to_country = (
                        row[0]
                        .find_all("div")[1]
                        .find_all("div")[1]
                        .find_all("span")[0]
                        .find_all("a")[1]
                    )
                    country_id, country_name = (
                        regex_country.findall(link_to_country.attrs["href"])[0],
                        link_to_country.attrs["data-original-title"],
                    )

                    # fix empty columns, set None
                    for i, el in enumerate(player_data):
                        if el == "" or el == "-":
                            player_data[i] = None

                    # no Attack_Defense_Work_Rate column from fifa10 to fifa14 included, so insert a None
                    if year <= 14:
                        player_data.insert(7, None)

                    # insert data extracted from links
                    player_data.insert(0, player_id)
                    player_data.insert(1, player_link_name)
                    player_data.insert(2, club_id)
                    player_data.insert(3, club_name)
                    player_data.insert(4, country_id)
                    player_data.insert(5, country_name)

                    buffer.append(player_data)

            if len(buffer) % 300 == 0:
                # write X lines at a time
                csv_writer.writerows(buffer)
                buffer = []

            # sleep random seconds between different pages
            wait = random.randint(10, 20)
            print(f"waiting {wait} seconds")
            time.sleep(wait)

        # if remaining lines write them
        if buffer:
            csv_writer.writerows(buffer)

    # sleep random seconds between different fifas
    wait = random.randint(300, 600)
    print(f"waiting {wait} seconds")
    time.sleep(wait)
