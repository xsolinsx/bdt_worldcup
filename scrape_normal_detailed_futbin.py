import csv
import random
import sqlite3
import time

import pandas as pd
import requests
from bs4 import BeautifulSoup

header = [
    "Player_ID",
    "Link_Name",
    "Birth_Date",
    "Foot",
    "Weight",
    "Pace_Acceleration",
    "Pace_Sprint_Speed",
    "Shooting_Positioning",
    "Shooting_Finishing",
    "Shooting_Shot_Power",
    "Shooting_Long_Shots",
    "Shooting_Volleys",
    "Shooting_Penalties",
    "Passing_Vision",
    "Passing_Crossing",
    "Passing_Free_Kick_Accuracy",
    "Passing_Short",
    "Passing_Long",
    "Passing_Curve",
    "Dribbling_Agility",
    "Dribbling_Balance",
    "Dribbling_Reactions",
    "Dribbling_Ball_Control",
    "Dribbling_Dribbling",
    "Dribbling_Composure",
    "Defending_Interceptions",
    "Defending_Heading_Accuracy",
    "Defending_Defensive_Awareness",
    "Defending_Standing_Tackle",
    "Defending_Sliding_Tackle",
    "Physicality_Jumping",
    "Physicality_Stamina",
    "Physicality_Strength",
    "Physicality_Aggression",
]
ds_dir = "./datasets/"

# run through all fifa versions of world cups
for year in range(10, 21, 4):
    # open a new csv with newline "\n" and encoding utf-8-sig to avoid text corruption
    with open(
        f"{ds_dir}normal_detailed_players{year}.csv", "w", newline="\n", encoding="utf-8-sig",
    ) as csv_file:
        csv_writer = csv.writer(csv_file)

        # write header
        csv_writer.writerow(header)

        conn = sqlite3.connect(f"{ds_dir}WorldCupPlayers{year}.db")
        df_fifa = pd.read_sql_query("SELECT * FROM NormalFifaPlayers", conn)
        conn.close()

        # list of rows to write
        buffer = []

        counter = 0
        for p_id, p_link_name in zip(df_fifa["Player_ID"], df_fifa["Link_Name"]):
            counter += 1
            print(f"fifa{year} {counter}/{len(df_fifa)} {p_link_name}")
            source = requests.get(
                f"https://www.futbin.com/{year}/player/{p_id}/{p_link_name}"
            ).text
            soup = BeautifulSoup(source, "lxml")

            # table to the left of the page, take all the rows
            physical_stats = soup.find("table", class_="table-info").find_all("tr")
            # div in the center, take all the rows
            detailed_stats = soup.find_all("div", class_="row_sep sub_stat")

            # always last tr and title attribute is always "DOB - DD-MM-YYYY"
            birth_date = physical_stats[-1].find("a").attrs["title"]
            # from fifa10 to fifa16 one field is missing, so indexes change
            if year < 17:
                # clean text to have "Foot Right/Left"
                foot = physical_stats[6].text.replace("\n", "").strip()
                # clean text to have "Weight N"
                weight = physical_stats[8].text.replace("\n", "").strip()
            else:
                # clean text to have "Foot Right/Left"
                foot = physical_stats[7].text.replace("\n", "").strip()
                # clean text to have "Weight N"
                weight = physical_stats[9].text.replace("\n", "").strip()

            # retrieve and clean all other fields to have "Field_X Value", then take only last part (Value)
            detailed_stats = [
                x.text.replace("\n", "").strip().split(" ")[-1] for x in detailed_stats
            ]

            player_detailed_data = [
                p_id,
                p_link_name,
                birth_date.split(" ")[-1],
                foot.split(" ")[-1],
                weight.split(" ")[-1],
            ]
            player_detailed_data.extend(detailed_stats)

            buffer.append(player_detailed_data)

            if len(buffer) % 300 == 0:
                # write X lines at a time
                csv_writer.writerows(buffer)
                buffer = []

            # sleep random seconds between different pages
            wait = random.randint(10, 20)
            print(f"waiting {wait} seconds")
            time.sleep(wait)

        # if remaining lines write them
        if buffer:
            csv_writer.writerows(buffer)

    # sleep random seconds between different fifas
    wait = random.randint(300, 600)
    print(f"waiting {wait} seconds")
    time.sleep(wait)
