import csv
import re

import requests
from bs4 import BeautifulSoup

header = [
    "Country",
    "Number",
    "Role",
    "Name",
    "Age",
    "Club",
]
links = [
    ("https://en.wikipedia.org/wiki/2010_FIFA_World_Cup_squads", 10),
    ("https://en.wikipedia.org/wiki/2014_FIFA_World_Cup_squads", 14),
    ("https://en.wikipedia.org/wiki/2018_FIFA_World_Cup_squads", 18),
]
ds_dir = "./datasets/"

regex_age = re.compile(r"aged (\d+)")

# run through all world cups
for link, year in links:
    source = requests.get(link).text
    soup = BeautifulSoup(source, "lxml")

    country_names = [
        x.text.strip()
        for i, x in enumerate(soup.find_all("span", class_="toctext")[:40])
        if i % 5 != 0  # exclude every 5 because of "Group X" link
    ]
    teams = zip(
        country_names,
        soup.find_all("table", class_="sortable wikitable plainrowheaders"),  # players
    )

    world_cup_players = []
    for team_name, team_table in teams:
        for tr in team_table.find_all("tr", class_="nat-fs-player"):
            row = list(filter(lambda x: x != "\n", tr.children))
            world_cup_players.append(
                [
                    team_name,  # country name
                    row[0].text.strip(),  # player's number
                    list(row[1].children)[1].text.strip(),  # role
                    list(row[2].children)[0]
                    .text.replace("(c)", "")
                    .replace("(captain)", "")
                    .strip(),  # name
                    int(regex_age.findall(row[3].text)[0].strip()),  # age
                    (row[5] if year < 18 else row[6]).text.strip(),  # player's club
                ]
            )

    # open a new csv with newline "\n" and encoding utf-8-sig to avoid text corruption
    with open(
        f"{ds_dir}wiki_players{year}.csv", "w", newline="\n", encoding="utf-8-sig",
    ) as csv_file:
        csv_writer = csv.writer(csv_file)
        # write header
        csv_writer.writerow(header)
        csv_writer.writerows(world_cup_players)
